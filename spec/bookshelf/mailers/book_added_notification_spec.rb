require_relative '../../spec_helper'

describe Mailers::BookAddedNotification do
  let(:subject) { Mailers::BookAddedNotification }

  before do
    Hanami::Mailer.deliveries.clear
  end

  it 'has correct `from` address' do
    subject.from.must_equal('no-reply@example.com')
  end

  it 'has correct `to` email address' do
    subject.to.must_equal('admin@example.com')
  end

  it 'has correct `subject`' do
    subject.subject.must_equal('Book added!')
  end

  it 'delivers email' do
    count =  Hanami::Mailer.deliveries.length
    subject.deliver
    assert (Hanami::Mailer.deliveries.length - count ) == 1
  end
end
