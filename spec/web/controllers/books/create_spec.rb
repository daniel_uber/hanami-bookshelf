require_relative '../../../spec_helper'

describe Web::Controllers::Books::Create do
  let(:interactor) { Minitest::Mock.new }
  let(:action) { Web::Controllers::Books::Create.new(interactor: interactor) }

  describe 'with valid params' do
    let(:params) { Hash[book: {title: 'Confident Ruby', author: 'Avdi Grimm'}] }

    before do
      interactor.expect(:call, true, [params.dig(:book)])
    end

    it 'calls interactor' do
      action.call(params)
      interactor.verify
    end

    it 'redirects the user to the books listing' do
      response = action.call(params)

      response[0].must_equal 302
      response[1]['Location'].must_equal '/books'
    end
  end

  describe 'with invalid params' do
    let(:params) { Hash[book: {}] }

    it 'returns HTTP client error' do
      response = action.call(params)

      response[0].must_equal 422
    end

    it 'dumps errors in params' do
      action.call(params)
      errors = action.params.errors

      errors.dig(:book, :title).must_equal ['is missing']
      errors.dig(:book, :author).must_equal ['is missing']
    end
  end
end
