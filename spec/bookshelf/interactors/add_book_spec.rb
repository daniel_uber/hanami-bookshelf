require 'spec_helper'

# following http://hanamirb.org/guides/1.1/architecture/interactors/
# but transcribing to minitest (since the getting started used minitest)
describe AddBook do
  let(:interactor) { AddBook.new }
  let(:attributes) { Hash[author: 'James Baldwin', title: 'The Fire Next Time'] }

  describe 'good input' do
    let(:result) { interactor.call(attributes) }

    it 'succeeds' do
      assert result.success?
    end

    it 'creates a Book with correct title and author' do
      result.book.title.must_equal('The Fire Next Time')
      result.book.author.must_equal('James Baldwin')
    end
  end

  describe 'persistence' do
    let(:repository) { Minitest::Mock.new }

    it 'persists the Book' do
      repository.expect(:create, true, [attributes])
      AddBook.new(repository: repository).call(attributes)
      repository.verify
    end
  end

  describe 'sending email' do
    let(:mailer) { Minitest::Mock.new }

    it 'sends :deliver to the mailer' do
      mailer.expect(:deliver, true, [])
      AddBook.new(mailer: mailer).call(attributes)
      mailer.verify
    end
  end
end
